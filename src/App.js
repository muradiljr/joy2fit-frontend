import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Header from './components/Header';
import LoginPage from './pages/login';
import HomePage from './pages/homePage';
import RegisterPage from './pages/register';
import MainPage from './pages/mainPage';

import "../src/css/index.css";

const App = () => {
    return (
        <Router>
            <Header />
            <Routes>
                <Route path={'/'} exact element={<HomePage />} />
                <Route path={'/main'} element={<MainPage />} />
                <Route path={'/login'} element={<LoginPage />} />
                <Route path={'/register'} element={<RegisterPage />} />
            </Routes>
        </Router>
    );
};

export default App;