import React from "react";
import { Link } from "react-router-dom";
import Logo from "../img/Logo.png";
import { User } from "phosphor-react";

const Header = () => {
    return (
        <header className="max-w-100% bg-light-blue xl:px-[20px]">
            <div className="max-w-1200px flex h-14 justify-between items-center m-auto">
                <Link to="/"><img src={Logo} alt="" /></Link>
                <Link to="/login" className="font-roboto text-16px items-center flex gap-2"><User size={16} />Войти</Link>
            </div>
        </header>
    )
}

export default Header;