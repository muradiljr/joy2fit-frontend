import React from "react";
import { Link } from "react-router-dom";

import TrainersPoster from '../homePage/images/trainers_poster.png';
import TrainerFace from '../homePage/images/trainer_face.png';
import FoodImage from '../homePage/images/food_image.png';
import SportsmanImageFirst from '../homePage/images/sportsman_image1.png';
import SportsmanImageSecond from '../homePage/images/sportsman_image2.png';

const HomePage = () => {
    return (
        <div className="max-w-1200px mx-auto my-[30px] bg-white rounded-[8px] xl:mx-[20px]">
            <div className="flex w-[100%] h-[450px] font-work-sans lg:flex-col lg:w-[100%] lg:h-[100vh]">
                <div className="w-[50%] ml-[40px] mt-[40px] flex flex-col lg:h-[100%] lg:w-[100%] lg:ml-0 lg:items-center xs:h-auto">
                    <p className="text-36px lg:text-center xs:text-[24px]">Мы собрали <span className="text-5ABAEA">лучших</span> тренеров для <span className="text-5ABAEA">Вас</span></p>
                    <p className="text-24px text-808080 mt-[70px] lg:text-center xs:text-[18px] xs:mt-[20px]">Наши профессиональные тренера помогут Вам достичь желаемых результатов</p>

                    <div className="flex mt-[70px] font-roboto xs:flex-col xs:mt-[20px]">
                        <Link to="" className="px-[30px] py-[11px] rounded-[8px] text-white hover:bg-white hover:text-5ABAEA border-[1px] ease-in-out duration-200 bg-5ABAEA mr-[25px] hover:border-solid hover:border-5ABAEA xs:mb-[20px] xs:mr-0">Подобрать тренера</Link>
                        <Link to="" className="px-[30px] py-[11px] rounded-[8px] text-5ABAEA hover:bg-5ABAEA hover:text-white border-[1px] ease-in-out duration-200 border-solid border-5ABAEA">Посмотреть всех</Link>
                    </div>
                </div>

                <div className="w-[50%] bg-trainers_ellipse_right bg-no-repeat bg-[length:100%_auto] bg-bottom relative flex lg:h-[100%] lg:w-[100%] xs:max-h-[45%]">
                    <img src={TrainersPoster} alt="" className="absolute bottom-0 left-0" />
                </div>
            </div>

            <div className="flex mt-[150px] justify-evenly md:flex-wrap md:mt-[50px] xs:mt-[20px]">
                <div className="border-t-[5px] border-solid border-5ABAEA w-[170px] md:my-[20px]">
                    <p className="font-work-roboto font-medium text-16px text-center">Подбери тренера</p>
                    <p className="font-work-roboto text-14px text-center text-808080">Сотни квалифицированных тренеров</p>
                </div>
                <div className="border-t-[5px] border-solid border-5ABAEA w-[170px] md:my-[20px]">
                    <p className="font-work-roboto font-medium text-16px text-center">Настрой питание</p>
                    <p className="font-work-roboto text-14px text-center text-808080">Множество программ питания</p>
                </div>
                <div className="border-t-[5px] border-solid border-5ABAEA w-[170px] md:my-[20px]">
                    <p className="font-work-roboto font-medium text-16px text-center">Набери массу</p>
                    <p className="font-work-roboto text-14px text-center text-808080">Программы тренировок на набор мышечной массы</p>
                </div>
                <div className="border-t-[5px] border-solid border-5ABAEA w-[170px] md:my-[20px]">
                    <p className="font-work-roboto font-medium text-16px text-center">Стань здоровее</p>
                    <p className="font-work-roboto text-14px text-center text-808080">Программы ОФП, уроки Йоги</p>
                </div>
            </div>

            <div className="flex flex-col mt-[50px] font-roboto justify-center xs:mt-[20px]">
                <h3 className="text-32px text-center">Подбери программу питания</h3>

                <div className="flex justify-evenly items-center my-[35px] mx-[100px] bg-light-blue p-[20px] xl:flex-wrap gap-[20px] xl:m-0">
                    <Link to=''>
                        <div className="">
                            <img src={FoodImage} alt="" />
                            <p className="text-20px text-808080 text-center font-roboto mt-[20px]">Стать стройнее</p>
                        </div>
                    </Link>

                    <Link to=''>
                        <div className="">
                            <img src={FoodImage} alt="" />
                            <p className="text-20px text-808080 text-center font-roboto mt-[20px]">Набрать вес</p>
                        </div>
                    </Link>

                    <Link to=''>
                        <div className="">
                            <img src={FoodImage} alt="" />
                            <p className="text-20px text-808080 text-center font-roboto mt-[20px]">Стать сильнее</p>
                        </div>
                    </Link>
                </div>
            </div>

            <div className="flex w-[100%] h-[450px] font-work-sans lg:flex-col lg:w-[100%] lg:h-[100vh] lg:mt-[20px]">
                <div className="w-[50%] bg-trainers_ellipse_left bg-no-repeat bg-[length:100%_auto] bg-bottom relative flex lg:h-[100%] lg:w-[100%] xs:max-h-[45%]">
                    <img src={TrainerFace} alt="" className="absolute bottom-0 left-0" />
                </div>

                <div className="w-[50%] ml-[40px] mt-[40px] flex flex-col lg:h-[100%] lg:w-[100%] lg:ml-0 lg:items-center">
                    <p className="text-36px lg:text-center xs:text-[24px]">Консультации со специалистом</p>
                    <p className="text-24px text-808080 mt-[70px] lg:text-center xs:text-[18px]">Наши профессиональные тренера помогут Вам достичь желаемых результатов</p>

                    <div className="flex mt-[70px] font-roboto xs:flex-col">
                        <Link to="" className="px-[30px] py-[11px] rounded-[8px] text-white hover:bg-white hover:text-5ABAEA border-[1px] ease-in-out duration-200 bg-5ABAEA mr-[25px] hover:border-solid hover:border-5ABAEA xs:mb-[20px] xs:mr-0">Подобрать тренера</Link>
                        <Link to="" className="px-[30px] py-[11px] rounded-[8px] text-5ABAEA hover:bg-5ABAEA hover:text-white border-[1px] ease-in-out duration-200 border-solid border-5ABAEA">Посмотреть всех</Link>
                    </div>
                </div>
            </div>

            <div className="flex flex-col mt-[50px] font-roboto justify-center">
                <h3 className="text-32px text-center">Подбери программу тренировок</h3>

                <div className="flex justify-evenly items-center my-[35px] mx-[100px] bg-light-blue p-[20px] xl:flex-wrap gap-[20px] xl:mb-[30px] xl:m-0">
                    <Link to=''>
                        <div className="">
                            <img src={SportsmanImageFirst} alt="" />
                            <p className="text-20px text-808080 text-center font-roboto mt-[20px]">Стать стройнее</p>
                        </div>
                    </Link>

                    <Link to=''>
                        <div className="">
                            <img src={SportsmanImageSecond} alt="" />
                            <p className="text-20px text-808080 text-center font-roboto mt-[20px]">Набрать вес</p>
                        </div>
                    </Link>

                    <Link to=''>
                        <div className="">
                            <img src={SportsmanImageFirst} alt="" />
                            <p className="text-20px text-808080 text-center font-roboto mt-[20px]">Стать сильнее</p>
                        </div>
                    </Link>
                </div>
            </div>


        </div>
    )
}

export default HomePage