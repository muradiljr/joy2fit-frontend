import React from "react";
import { Link } from "react-router-dom";

import {Check, Lock, UserCirclePlus} from "phosphor-react";
import vk from "../../img/VK.svg";
import insta from "../../img/Instagram.svg";
import facebook from "../../img/Facebook.svg";
import google from "../../img/Google.svg";

const LoginPage = () => {
    return (
        <div className="max-w-1200px mx-auto flex justify-center items-center h-screen">
            <div className="m-auto w-3/6 bg-white h-[360px] rounded-lg md:h-[520px] md:mt-[35px] md:mb-[35px] sm:w-4/5">
                <div className="flex items-center gap-2 px-2 py-2 border-b border-light-gray">
                    <Check className="text-semi-light-blue" size={16}/><span>Вход</span>
                </div>
                <div className="flex px-4 py-8 justify-around md:justify-start md:flex-col">
                    <div className="w-3/6 px-4 text-14px md:w-full">
                        <form>
                            <label htmlFor="username" className="relative block">Email
                                <span className="absolute top-[39px] left-0 flex items-center pl-2">
                                <UserCirclePlus size={32} className="h-5 w-5 text-gray"/>
                            </span>
                                <input
                                    className="w-full outline-none border rounded-lg border-slate-400 h-9 mt-[8px] mb-[14px] py-2 pl-8 pr-3"
                                    type="email" id="username" placeholder="Введите email" required/>
                            </label>
                            <label htmlFor="password" className="relative block">Password
                                <span className="absolute top-[39px] left-0 flex items-center pl-2">
                                <Lock size={32} className="h-5 w-5 text-gray"/>
                            </span>
                                <input
                                    className="w-full outline-none border rounded-lg border-slate-400 h-9 mt-[8px] mb-[14px] py-2 pl-8 pr-3"
                                    type="password" id="password" placeholder="Введите пароль" required/>
                            </label>
                            <button className="w-full text-center bg-semi-light-blue rounded-lg py-2.5 text-white">Войти</button>
                        </form>
                        <div className="text-12px mt-[11px] text-semi-light-blue">
                            <p><Link to="/reset-password">Забыли пароль?</Link></p>
                            <p className="text-gray mt-[11px]">Еще не с нами? <span className="text-semi-light-blue"><Link to="/register">Зарегистрируйся!</Link></span></p>
                        </div>
                    </div>
                    <div className="w-1/2 flex justify-center items-center md:w-full md:pt-4">
                        <div className="w-full text-center rounded-lg h-[250px] bg-bold-light-blue md:h-[140px]">
                            <div className="grid grid-cols-1 place-items-center h-[250px] place-content-center md:h-[140px]">
                                <p>Войти через соцсети</p>
                                <div className="gap-2 flex px-1.5 mt-[30px]">
                                    <img className="cursor-pointer" src={vk} alt="vk"/>
                                    <img className="cursor-pointer" src={insta} alt="insta"/>
                                    <img className="cursor-pointer" src={facebook} alt="facebook"/>
                                    <img className="cursor-pointer" src={google} alt="google"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginPage;