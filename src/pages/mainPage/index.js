import React from "react";
import { Link } from "react-router-dom";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from "swiper";

import "swiper/css";
import "swiper/css/pagination";

import { Star, ChatText, ArrowDown } from "phosphor-react";

import Trainer1 from "../mainPage/images/trainer_top-1.png"
import Trainer2 from "../mainPage/images/trainer_top-2.png"
import Trainer3 from "../mainPage/images/trainer_top-3.png"

import FullStar from "../mainPage/images/star_24px.png"
import HalfStar from "../mainPage/images/star_half_24px.png"
import EmptyStar from "../mainPage/images/star_border_24px.png"

import SmallIvanImage from "../mainPage/images/small-ivan-image.png"
import SmallYuriiImage from "../mainPage/images/small-yurii-image.png"
import SmallAnnaImage from "../mainPage/images/small-anna-image.png"
import SmallKhlopkovImage from "../mainPage/images/small-yurii-hlopkov-image.png"

const MainPage = () => {
    return (
        // Родительский блок всей разметки на этой странице
        <div className="max-w-1200px mx-auto flex justify-center font-roboto mt-[30px] xl:flex-wrap lg:w-[95vw] sm:w-[90vw]">
            {/* Начало блока со слайдером */}
            <div className="mx-auto my-0 w-[780px] h-[360px] bg-white rounded-lg border-DADAD lg:mx-0 lg:w-[100%]">
                {/* Начало блока с заголовком "Топ тренеров" и иконкой */}
                <div className="flex items-center gap-2 px-2 py-2 border-b border-gray-200">
                    <Star className="text-5ABAEA" size={20} /><span className="text-14px text-000000">Топ тренеров</span>
                </div>
                {/* Конец блока с заголовком "Топ тренеров" и иконкой */}


                <div className="pt-[15px]">
                    <Swiper
                        slidesPerView={1}
                        spaceBetween={10}

                        navigation={true}
                        pagination={{ clickable: true }}
                        breakpoints={{
                            525: {
                                slidesPerView: 2,
                                spaceBetween: 10,
                            },
                            640: {
                                slidesPerView: 2,
                                spaceBetween: 10,
                            },
                            768: {
                                slidesPerView: 3,
                                spaceBetween: 10,
                            },
                            1024: {
                                slidesPerView: 3,
                                spaceBetween: 15,
                            },
                        }}
                        modules={[Pagination]}
                        className="mySwiper h-[300px]"
                    >
                        <SwiperSlide className="flex justify-center">
                            <div>
                                <img src={Trainer1} alt="" />
                                <p className="trainer-name text-center mt-[10px]">Юрий Краснокукан</p>
                                <div className="trainer-rating flex flex-row justify-center">
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={HalfStar} alt="" className="mr-[8px]" />
                                    <img src={EmptyStar} alt="" />
                                </div>

                                <div className="trainer-xpAndAge flex flex-row items-center justify-between mt-[10px]">
                                    <span className="text-12px">Стаж: 2 года</span>
                                    <span className="text-12px">Возраст: 38 лет</span>
                                </div>
                            </div>
                        </SwiperSlide>

                        <SwiperSlide className="flex justify-center">
                            <div>
                                <img src={Trainer1} alt="" />
                                <p className="trainer-name text-center mt-[10px]">Юрий Краснокукан</p>
                                <div className="trainer-rating flex flex-row justify-center">
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={HalfStar} alt="" className="mr-[8px]" />
                                    <img src={EmptyStar} alt="" />
                                </div>

                                <div className="trainer-xpAndAge flex flex-row items-center justify-between mt-[10px]">
                                    <span className="text-12px">Стаж: 7 лет</span>
                                    <span className="text-12px">Возраст: 32 года</span>
                                </div>
                            </div>
                        </SwiperSlide>

                        <SwiperSlide className="flex justify-center">
                            <div>
                                <img src={Trainer1} alt="" />
                                <p className="trainer-name text-center mt-[10px]">Юрий Краснокукан</p>
                                <div className="trainer-rating flex flex-row justify-center">
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={HalfStar} alt="" className="mr-[8px]" />
                                    <img src={EmptyStar} alt="" />
                                </div>

                                <div className="trainer-xpAndAge flex flex-row items-center justify-between mt-[10px]">
                                    <span className="text-12px">Стаж: 12 лет</span>
                                    <span className="text-12px">Возраст: 35 лет</span>
                                </div>
                            </div>
                        </SwiperSlide>

                        <SwiperSlide className="flex justify-center">
                            <div>
                                <img src={Trainer1} alt="" />
                                <p className="trainer-name text-center mt-[10px]">Юрий Краснокукан</p>
                                <div className="trainer-rating flex flex-row justify-center">
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={HalfStar} alt="" className="mr-[8px]" />
                                    <img src={EmptyStar} alt="" />
                                </div>

                                <div className="trainer-xpAndAge flex flex-row items-center justify-between mt-[10px]">
                                    <span className="text-12px">Стаж: 4 года</span>
                                    <span className="text-12px">Возраст: 27 лет</span>
                                </div>
                            </div>
                        </SwiperSlide>

                        <SwiperSlide className="flex justify-center">
                            <div>
                                <img src={Trainer1} alt="" />
                                <p className="trainer-name text-center mt-[10px]">Юрий Краснокукан</p>
                                <div className="trainer-rating flex flex-row justify-center">
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={HalfStar} alt="" className="mr-[8px]" />
                                    <img src={EmptyStar} alt="" />
                                </div>

                                <div className="trainer-xpAndAge flex flex-row items-center justify-between mt-[10px]">
                                    <span className="text-12px">Стаж: 5 лет</span>
                                    <span className="text-12px">Возраст: 31 год</span>
                                </div>
                            </div>
                        </SwiperSlide>

                        <SwiperSlide className="flex justify-center">
                            <div>
                                <img src={Trainer1} alt="" />
                                <p className="trainer-name text-center mt-[10px]">Юрий Краснокукан</p>
                                <div className="trainer-rating flex flex-row justify-center">
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={FullStar} alt="" className="mr-[8px]" />
                                    <img src={HalfStar} alt="" className="mr-[8px]" />
                                    <img src={EmptyStar} alt="" />
                                </div>

                                <div className="trainer-xpAndAge flex flex-row items-center justify-between mt-[10px]">
                                    <span className="text-12px">Стаж: 6 лет</span>
                                    <span className="text-12px">Возраст: 39 лет</span>
                                </div>
                            </div>
                        </SwiperSlide>
                    </Swiper>
                </div>
            </div>
            {/* Конец блока со слайдером */}

            {/* Начало блока "Последние отзывы" с иконкой */}
            <div className="user-reviews w-[400px] bg-white rounded-lg lg:mt-[20px]">
                <div className="flex items-center gap-2 px-2 py-2 border-b border-gray-200">
                    <ChatText className="text-5ABAEA" size={20} /><span className="text-14px text-000000">Последние отзывы</span>
                </div>

                <div className="review p-[8px]">
                    <div className="flex">
                        <div className="review-trainer-foto w-[30%]">
                            <img src={SmallIvanImage} alt="" className="w-[100%]" />
                        </div>

                        <div className="review-user ml-[10px]">
                            <span className="review-trainer-name">Иван Бер</span>
                            <p className="review-user-words text-14px font-light ">Отличный тренер. Составили программу питания и тренировок. Рекомендую!</p>
                        </div>
                    </div>

                    <div className="review-rating flex justify-between items-center">
                        <div className="review-rating-stars flex flex-row">
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={HalfStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={EmptyStar} alt="" className="w-[16px] h-[16px]" />
                        </div>
                        <div className="review-time-user">
                            <span className="text-12px font-roboto font-light">12.06.2022</span>
                            <span className="text-14px ml-[8px] text-5ABAEA xs:text-12px">Сергей Иванов</span>
                        </div>
                    </div>
                </div>

                <div className="review p-[8px]">
                    <div className="flex">
                        <div className="review-trainer-foto w-[30%]">
                            <img src={SmallIvanImage} alt="" className="w-[100%]" />
                        </div>

                        <div className="review-user ml-[10px]">
                            <span className="review-trainer-name">Иван Бер</span>
                            <p className="review-user-words text-14px font-light">Отличный тренер. Составили программу питания и тренировок. Рекомендую!</p>
                        </div>
                    </div>

                    <div className="review-rating flex justify-between items-center">
                        <div className="review-rating-stars flex flex-row">
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={HalfStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={EmptyStar} alt="" className="w-[16px] h-[16px]" />
                        </div>
                        <div className="review-time-user">
                            <span className="text-12px font-roboto font-light">12.06.2022</span>
                            <span className="text-14px ml-[8px] text-5ABAEA xs:text-12px">Сергей Иванов</span>
                        </div>
                    </div>
                </div>

                <div className="review p-[8px]">
                    <div className="flex">
                        <div className="review-trainer-foto w-[30%]">
                            <img src={SmallIvanImage} alt="" className="w-[100%]" />
                        </div>

                        <div className="review-user ml-[10px]">
                            <span className="review-trainer-name">Иван Бер</span>
                            <p className="review-user-words text-14px font-light">Отличный тренер. Составили программу питания и тренировок. Рекомендую!</p>
                        </div>
                    </div>

                    <div className="review-rating flex justify-between items-center">
                        <div className="review-rating-stars flex flex-row">
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={HalfStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={EmptyStar} alt="" className="w-[16px] h-[16px]" />
                        </div>
                        <div className="review-time-user">
                            <span className="text-12px font-roboto font-light">12.06.2022</span>
                            <span className="text-14px ml-[8px] text-5ABAEA xs:text-12px">Сергей Иванов</span>
                        </div>
                    </div>
                </div>

                <div className="review p-[8px]">
                    <div className="flex">
                        <div className="review-trainer-foto w-[30%]">
                            <img src={SmallIvanImage} alt="" className="w-[100%]" />
                        </div>

                        <div className="review-user ml-[10px]">
                            <span className="review-trainer-name">Иван Бер</span>
                            <p className="review-user-words text-14px font-light">Отличный тренер. Составили программу питания и тренировок. Рекомендую!</p>
                        </div>
                    </div>

                    <div className="review-rating flex justify-between items-center">
                        <div className="review-rating-stars flex flex-row">
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={FullStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={HalfStar} alt="" className="mr-[8px] w-[16px] h-[16px]" />
                            <img src={EmptyStar} alt="" className="w-[16px] h-[16px]" />
                        </div>
                        <div className="review-time-user">
                            <span className="text-12px font-roboto font-light">12.06.2022</span>
                            <span className="text-14px ml-[8px] text-5ABAEA xs:text-12px">Сергей Иванов</span>
                        </div>
                    </div>
                </div>

                <div className="new-reviews text-center mb-[8px] mx-[8px]">
                    <Link to="" className="py-[8px] text-center block bg-light-blue text-14px rounded-[8px] flex items-center"><ArrowDown size={20} className="text-5ABAEA ml-[8px] w-[20px] h-[20px]" /><p className="w-[100%]">Показать еще отзывы</p></Link>
                </div>
            </div>
            {/* Начало блока "Последние отзывы" с иконкой */}
        </div>
        // Конец родительского блока всей разметки на этой странице
    )
}

export default MainPage;