import React from "react";
import { Link } from "react-router-dom";
import { At, CalendarBlank, DownloadSimple, Lock, PlusCircle, UserCircle, Users } from "phosphor-react";

const RegisterPage = () => {
    return (
        <form action="" className="mt-[90px]">
            <div className="max-w-1200px mx-auto flex justify-center items-center font-roboto">
                <div className="mx-auto my-0 w-[600px] bg-white rounded-lg border-DADADA sm:w-[90vw]">
                    <div className="flex items-center gap-2 px-2 py-2 border-b border-gray-200">
                        <PlusCircle className="text-5ABAEA" size={15} /><span className="text-14px text-000000">Регистрация</span>
                    </div>

                    <div className="flex px-[15px] py-[20px] justify-between flex-wrap sm:flex-col">
                        <div className="w-[50%] px-4 gap-2 sm:w-[100%]">
                            <div className="mb-4 relative">
                                <label htmlFor="email" className="text-12px">Email</label>
                                <At size={20} className="absolute top-8 left-2" />
                                <input className="w-full outline-none border rounded-lg border-slate-400 h-9 text-14px pl-8" type="email" id="email" placeholder="Введите email" />
                            </div>

                            <div className="mb-4 relative">
                                <label htmlFor="user-first-name" className="text-12px">Имя</label>
                                <UserCircle size={20} className="absolute top-8 left-2" />
                                <input className="w-full outline-none border rounded-lg border-slate-400 h-9 text-14px pl-8" type="text" id="user-first-name" placeholder="Введите Ваше имя" />
                            </div>

                            <div className="mb-4 relative">
                                <label htmlFor="user-last-name" className="text-12px">Фамилия</label>
                                <Users size={20} className="absolute top-8 left-2" />
                                <input className="w-full outline-none border rounded-lg border-slate-400 h-9 text-14px pl-8" type="text" id="user-last-name" placeholder="Введите фамилию" />
                            </div>

                            <div className="mb-4 relative">
                                <label className="text-12px">Пол</label>
                                <div className="flex items-center w-full">
                                    <input type="radio" id="male" name="sex" value="male" className="w-4 h-4 mr-2" />
                                    <label htmlFor="male" className="mr-4">Мужской</label>

                                    <input type="radio" id="female" name="sex" value="female" className="w-4 h-4 mr-2" />
                                    <label htmlFor="female">Женский</label>
                                </div>
                            </div>

                            <div className="mb-4 relative">
                                <label className="text-12px">Тренер?</label>
                                <div className="flex items-center w-full">
                                    <input type="checkbox" id="trainer" name="trainer" className="w-4 h-4 mr-2" />
                                    <label htmlFor="trainer" className="text-12px">Я тренер</label>
                                </div>
                                <p className="text-10px mt-2">Отметив этот пункт, Вы должны будете подтвердить, что вы являетесь тренером. Иначе аккаунт будет удален в течении 30 дней.</p>
                            </div>

                        </div>
                        {/* Левая часть формы */}

                        <div className="w-[50%] px-4 gap-2 sm:w-[100%]">
                            <div className="mb-4 relative">
                                <label htmlFor="password" className="text-12px">Пароль</label>
                                <Lock size={20} className="absolute top-8 left-2" />
                                <input className="w-full outline-none border rounded-lg border-slate-400 h-9 text-14px pl-8" type="password" id="password" placeholder="Введите пароль" />
                            </div>

                            <div className="mb-4 relative">
                                <label htmlFor="confirmPassword" className="text-12px">Подтверждение пароля</label>
                                <Lock size={20} className="absolute top-8 left-2" />
                                <input className="w-full outline-none border rounded-lg border-slate-400 h-9 text-14px pl-8" type="password" id="confirmPassword" placeholder="Введите пароль" />
                            </div>

                            <div className="mb-4 relative">
                                <label htmlFor="date" className="text-12px">Дата рождения</label>
                                <CalendarBlank size={20} className="absolute top-8 left-2" />
                                <input className="w-full outline-none border rounded-lg border-slate-400 h-9 text-14px pl-8" type="date" id="date" placeholder="Выбрать" />
                            </div>

                            <div className="mb-4">
                                <label htmlFor="profile_pic" className="text-12px">Фотография</label>
                                <div className="flex w-full mt-4 sm:justify-evenly">
                                    <label className="w-[100px] h-[100px] bg-light-blue cursor-pointer rounded-[50%] relative">
                                        <input type="file" id="profile_pic" name="profile_pic"
                                            accept=".jpg, .jpeg, .png" className="cursor-pointer hidden" />
                                        <DownloadSimple size={22} className="absolute top-[40%] left-[40%]" />
                                    </label>

                                    <p className="text-10px w-[148px] text-808080 ml-1 sm:w-[50%]">Загружаемый файл не должен превышать размер 3 MB. <br /> Загружаемый файл не должен быть более 500х500 px.</p>
                                </div>
                            </div>
                        </div>
                        {/* Правая часть формы */}

                        <div className="w-[50%] px-4 gap-2 sm:w-[100%]">
                            <div className="mb-4 relative">
                                <input type="submit" value="Зарегистрироваться" className="w-full py-[11px] bg-5ABAEA rounded-[8px] cursor-pointer text-white" />
                            </div>

                            <div className="mb-4 relative">
                                <span className="text-12px">Уже с нами? <Link to="/login" className="text-5ABAEA">Войти!</Link></span>
                            </div>
                        </div>
                        {/* Кнопка 'Зарегистрироваться' и ссылка на вход */}
                    </div>

                </div>
            </div>
        </form>
    )

}

export default RegisterPage;