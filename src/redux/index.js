import {combineReducers, configureStore} from "@reduxjs/toolkit";
import redux from "./reducers";

const rootReducer = combineReducers({
    toolkit: redux,
})

export const store = configureStore({
    reducer: rootReducer
})