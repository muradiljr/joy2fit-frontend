import {createSlice} from "@reduxjs/toolkit";

const reduxSlice = createSlice({
    name: "slice",
    initialState: {
        users: [],
    },
    reducers:{
        getUsers(state, action){
            state.users = action.payload
        }
    }
});

export default reduxSlice.reducer;
export const {getUsers} = reduxSlice.actions;