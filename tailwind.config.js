module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    fontFamily: {
      'work-sans': ['Work Sans', 'sans-serif'],
      'roboto': ['Roboto', 'sans-serif'],
    },

    fontSize: {
      '10px': '10px',
      '12px': '12px',
      '14px': '14px',
      '16px': '16px',
      '20px': '20px',
      '24px': '24px',
      '32px': '32px',
      '36px': '36px',
    },

    screens: {
      'xl': { 'max': '1280px' },
      'lg': { 'max': '1024px' },
      'md': { 'max': '800px' },
      'sm': { 'max': '650px' },
      'xs': { 'max': '576px' },
    },

    maxWidth: {
      '100%': '100%',
      '1200px': '1200px',
    },

    extend: {
      colors: {
        'light-blue': '#D0ECFA',
        'semi-light-blue': '#5ABAEA',
        'bold-light-blue': '#D0ECFA',
        'gray': '#808080',
        'light-gray': '#F2F2F2',
        'white': '#FFFFFF',
        'DADADA': '#DADADA',
        '5ABAEA': '#5ABAEA',
        'black': '#000000',
        '808080': '#808080',
      },

      backgroundImage: {
        'trainers_ellipse_right': "url('../img/trainers_ellipse_right.png')",
        'trainers_ellipse_left': "url('../img/trainers_ellipse_left.png')",
      }
    },
  },
  plugins: [],
}
